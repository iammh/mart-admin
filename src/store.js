import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import category from './states/category';
import products from './states/products';

export default new Vuex.Store({
  modules: {
    category: category,
    products: products
  }
})
