import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import underscore from 'vue-underscore'

Vue.config.productionTip = false

Vue.use(underscore)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
