import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/brands',
      name: 'brands',
      component: () => import('./views/Brands.vue')
    },
    {
          path: '/categories',
          name: 'categories',
          component: () => import('./views/Category.vue')
    },
    {
        path: '/payments',
        name: 'payments',
        component: () => import('./components/brand/Home.vue')
    },
    {
        path: '/customers',
        name: 'customers',
        component: () => import('./components/brand/Home.vue')
    },
    {
        path: '/products',
        name: 'products',
        component: () => import('./views/Products')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./views/Login')
    }
  ]
})
