import axios from 'axios';

function CategoryService (url,options) {
    this.url = url;
    this.options = options || {};
}

CategoryService.prototype.get = function () {
  return axios.get(this.url, this.options);
};

CategoryService.prototype.post = function (data) {
    return axios.post(this.url, data,this.options);
};

CategoryService.prototype.delete = function (data) {
    return axios.delete(this.url+"/"+data,this.options);
};

export default CategoryService