import CategoryService from "../../api/categoryService";
import {_} from 'vue-underscore';

export default {
    add(context, payload) {

        let myPromise = new Promise(function (resolve, reject) {
            const categoryService = new CategoryService('http://api.mart.io/v1/categories', {
                header: {
                    'Content-Type': 'application/json'
                }
            });
            categoryService.post(payload).then((response)=>{
                // console.log(data);
                if(response.data.success) {
                    console.log("Create post")
                    context.commit('category', response.data.data);
                    window.application.$emit('CLOSE_CREATE_DIALOG');
                    resolve(true);
                }

            }).catch((err) => {
                reject(err);
            })
        });



        return myPromise;

    },
    categories(context) {

        const categoryService = new CategoryService('http://api.mart.io/v1/categories');
        categoryService.get().then((response)=>{
            // console.log(data);
            context.state.categories = response.data;
        }).catch((err) => {console.log("Error ", err)})
    },
    update() {

    },
    removeCategory(context, id) {

        const categoryService = new CategoryService('http://api.mart.io/v1/categories', {
            header: {
                'Content-Type': 'application/json'
            }
        });
        categoryService.delete(id).then((response)=>{
            // console.log(data);
            if(response.data.success) {
                window.application.$emit('REMOVE_CATEGORY');
                context.commit('remove', id);
            }

        }).catch((err) => {
            console.log(err)
        })

    },
    getCategoryById(context, id) {
        return _.findWhere(context.state.categories, {id: id});
    }
}