import {_} from 'vue-underscore';

export default {
    category(state, value) {
        console.info("Category Mutation:: ", value);
        state.categories.push(value)
    },
    remove(state, value) {
        let item = _.findIndex(state.categories, function (o) {
            return o.id == value;
        });
       // state.categories.splice('')
        state.categories.splice(item, 1);
    }
}