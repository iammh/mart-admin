import state from "./state";
import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";

export default {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions

}